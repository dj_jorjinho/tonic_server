<?php

class View {
	
	private $data = null;
	private $viewname = null;
	private $viewpath = null;
	
	static function mk($name=null,$data=array()){
		return new View($name,$data);
	}
	
	function __construct($name=null,$data=array())
	{
		if(is_null($name))
			throw new Exception('Invalid View name!');
		
		$this->viewname = $name;
		$this->data = $data;
		return $this;
	}
	
	function __set($name,$value){
		return $this->set($name,$value);
	}
	
	function set($name,$value){
		$this->data[$name] = $value;
		return $this;
	}
	
	function get($name){
		if(key_exists($name,$this->data))
			return $this->data[$name];
		return null;
	}
	
	function render(){
		extract($this->data);
		ob_start();
		include $this->viewpath.'/'.$this->viewname.'.php';
		$_str = ob_get_contents();
		ob_end_clean();
		return $_str;
	}
	
	function setViewPath($path){
		$this->viewpath = realpath($path);
		return $this;
	}
	
	function __toString(){
		return $this->render();
	}
	
	function __call($name,$args){
		return $this->get($name);
	}
}