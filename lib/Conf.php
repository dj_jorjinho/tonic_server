<?php

class Conf {
	private static $config = array();
	
	/**
	 * Load config file
	 *
	 */
	static function mk($name='config'){
		if(!empty(self::$config[$name]))
			return Arr::mk(self::$config[$name]);
		
		// untracked config file
		$tryfile = '../conf/'.$name.'_inc.yaml';
		
		$filename = '../conf/'.$name.'.yaml';
		
		self::$config[$name] = Spyc::YAMLLoad($filename);
		
		// recursive merge with untracked file
		if(file_exists($tryfile))
			self::$config[$name] = array_merge_recursive(self::$config[$name]
													,Spyc::YAMLLoad($tryfile));
		
		return Arr::mk(self::$config[$name]);
	}
}