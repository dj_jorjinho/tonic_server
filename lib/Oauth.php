<?php

class Oauth {
	private $store;
	private $server;
	
	function __construct($conf){
		
		$conn = mysql_connect($conf->host, $conf->user, $conf->pass);
		if(!$conn)
			die(mysql_error());
		
		mysql_select_db($conf->db,$conn);
		
		$this->store = OAuthStore::instance('MySQL', array('conn' => $conn));
		$this->server = new OAuthServer();
	}
	
	function updateConsumer($data=array(), $user_id=null, $admin=false){
		return $this->store->updateConsumer($data, $user_id, $admin);
	}
	
	function getConsumer($key=null, $user_id){
		return $this->store->getConsumer($key,$user_id);
	}
	
	function listConsumers($user_id){
		$list = $this->store->listConsumers($user_id);
		return $list;
	}
	
	function requestToken(){
		return $this->server->requestToken();
	}
	
	function authorizeVerify(){
		return $this->server->authorizeVerify();
	}
	
	function authorizeFinish($authorized=false,$user_id){
		return $this->server->authorizeFinish($authorized,$user_id);
	}
	
	function accessToken(){
		return $this->server->accessToken();
	}
	
	function updateServer($server=array(),$user_id){
		return $this->store->updateServer($server,$user_id);
	}
	
	function request($request_uri=null,$method='GET',$params=array()){
		$req = new OAuthRequester($request_uri, 'GET', $params);
		$user_id = Arr::mk($_SESSION)->get('user_id');
		return $req->doRequest($user_id);
	}
}