<?php
class OauthCallback extends Controller{
	
	function get(){
		$this->oauth(); // start oauth
		
		// Request parameters are oauth_token, consumer_key and usr_id.
		$consumer_key = Arr::mk($_REQUEST)->get('consumer_key');
		$oauth_token = Arr::mk($_REQUEST)->get('oauth_token');
		$user_id = Arr::mk($_REQUEST)->get('usr_id');
		try
		{
			OAuthRequester::requestAccessToken($consumer_key
											   , $oauth_token, $user_id);
			
			$this->response()->code = Response::FOUND;
			$this->response()->addHeader('Location', 'http://tonic-server/');
		}
		catch (OAuthException $e)
		{
			$this->response()->code = Response::BADREQUEST;
			$this->response()->body = $e->getMessage();
		}
	}
	
}