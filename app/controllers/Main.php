<?php

class Main extends Controller{
	
	function get(){
		RLog::debug('here');
		$view = self::view()
					->set('num',Arr::mk($_SESSION)->get('user_id'));
		$this->response()->body = $view->render();
	}
	
}