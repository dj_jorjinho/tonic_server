<?php

class OauthRegister extends Controller{
	
	function post(){
		
		try{
			$key = $this->oauth()
					->updateConsumer($_REQUEST
									,Arr::mk($_SESSION)->get('user_id'));

			$c = $this->oauth()
					->getConsumer($key,Arr::mk($_SESSION)->get('user_id'));

			$this->response()->body = json_encode(array(
							'key' => $c['consumer_key'],
							'secret' => $c['consumer_secret']
						));
		}
		catch(OAuthException2 $e){
			$this->response()->code = Response::BADREQUEST;
			$this->response()->body = $e->getMessage();
		}
		
	}
	
	
	
}